import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class BaseTest {
    private AndroidDriver driver;
    private String parenPath = "/jenkins-slave/workspace/mb-sales-android-ci-build/app/build/outputs/apk/debug";
//    private String parenPath = "/home/gagan/Documents/msa/mb-sales-android/app/build/outputs/apk/debug";
    private String apkName = "app-debug.apk";
    private String deviceName = "Nexus_5X_API_26";
    private String url = "http://0.0.0.0:4723/wd/hub";
    private final String androidId = "com.mathrubhumi.salesapp:id/";
    private final String packageAllowButtton="com.android.packageinstaller:id/permission_allow_button";
    private final String packageDenayButtton="com.android.packageinstaller:id/permission_deny_button";
    private final String switchOverlay="com.android.settings:id/switch_widget";
    @Before
    public void setUp() {
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("BROWSER_NAME", "Android");
        dc.setCapability(MobileCapabilityType.PLATFORM_NAME,
                Platform.ANDROID);
        dc.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
        dc.setCapability("autoGrantPermissions", "true");

        File apkFile = new File(
                parenPath,
                apkName);
        dc.setCapability(MobileCapabilityType.APP, apkFile.getAbsolutePath());

        try {
            driver = new AndroidDriver(
                    new URL(url), dc);
            System.out.println(driver.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void SimpleTest1() {
        MobileElement likeButton = (MobileElement) driver.findElement(By.id(androidId + "iv_like"));
        likeButton.click();
//        MobileElement signinButton = (MobileElement) driver.findElement(By.name("Sign in with Google"));
//        signinButton.click();

    }

}
